#include "RequestsManager.h"
#include "HandshakeRequest.h"
#include "SumRequest.h"
#include "WordCountRequest.h"
#include "AstroSignRequest.h"

#include "boost/date_time/gregorian/gregorian.hpp"
#include <iostream>

RequestsManager::RequestsManager()
{
	// this->requests.emplace("Handshake", std::make_shared<HandshakeRequest>());
	// this->requests.emplace("Sum", std::make_shared<SumRequest>());
	// this->requests.emplace("WordCounter", std::make_shared<WordCountRequest>());

	requestAstroSign();

}

void RequestsManager::requestAstroSign()
{
	int luna, zi, an;
	std::cout << "Introduceti Data de nastere cu urmatorul format: luna zi an ";
	std::cin >> luna >> zi >> an;
	// Using Client-Side Verification

	while (isDateCorrect(luna, zi, an) == false)
	{
	std::cout << "Introduceti o data corecta! : luna zi an" << std::endl;
	std::cin >> luna >> zi >> an;
	}

	this->requests.emplace("AstroSign", std::make_shared<AstroSignRequest>(luna, zi, an));
}

bool RequestsManager::isDateCorrect(int luna, int zi, int an)
{
		try
		{
			boost::gregorian::date date(an, luna, zi);
		}
		catch (std::out_of_range)
		{
			std::cout << "Error, Date does not exist. Try again" << std::endl;
			return false;
		}
	return true;
}

std::map<std::string, std::shared_ptr<Framework::Request>> RequestsManager::getMap() const
{
	return this->requests;
}
